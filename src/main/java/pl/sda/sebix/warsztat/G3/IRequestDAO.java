package pl.sda.sebix.warsztat.G3;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.sda.sebix.warsztat.model.Request;

@Repository
public interface IRequestDAO extends CrudRepository<Request, Long> {
}
