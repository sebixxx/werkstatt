package pl.sda.sebix.warsztat.G3;

import java.util.List;

public interface IRequestList {

    void addRequest(String name);

    void deleteRequest(String name);

    List getAllRequests();


}
