package pl.sda.sebix.warsztat.G3;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import pl.sda.sebix.warsztat.model.Request;
import pl.sda.sebix.warsztat.model.Workshop;
import pl.sda.sebix.warsztat.workshop.IWorkshopDAO;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
@CrossOrigin
@RestController
public class RequestController {

    @Autowired
    IRequestDAO iRequestDAO;



    @GetMapping("/request/list")
    @Transactional
    @ResponseBody
    public ResponseEntity<ArrayList<Request>> getAllRequests() {
        Iterable<Request> all = iRequestDAO.findAll();
        ArrayList<Request> requests = new ArrayList<>();
        for (Request r : all) {
            requests.add(r);
        }
        return ResponseEntity.ok(requests);
    }

    @GetMapping("/request/add/{name:[A-Za-z0-9 ]+}")
    @Transactional
    @ResponseBody
    public ResponseEntity<Request> addRequest(@PathVariable("name") String name, ModelMap modelMap) {
        Request request = new Request();
        request.setName(name);
        iRequestDAO.save(request);
        return  ResponseEntity.ok(request);
    }

    @GetMapping("/request/remove/{id:[0-9]+}")
    @Transactional
    @ResponseBody
    public ResponseEntity<Request> removeRequest(@PathVariable("id") String id, ModelMap model) {
        Request requestToDelete = iRequestDAO.findOne(Long.valueOf(id));
        iRequestDAO.delete(Long.valueOf(id));
        return ResponseEntity.ok(requestToDelete);
    }


}