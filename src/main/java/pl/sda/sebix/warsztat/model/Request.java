package pl.sda.sebix.warsztat.model;

import org.springframework.context.annotation.Scope;

import javax.persistence.*;

@Entity
@Scope("prototype")
@Table(name = "request_table")
public class Request {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "request_id")
    private Long id;
    @Column(name = "request_name")
    private String requestName;

    public Request() {
    }

    public String getName() {
        return requestName;
    }

    public void setName(String name) {
        this.requestName = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
