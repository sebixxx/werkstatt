package pl.sda.sebix.warsztat.car;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import pl.sda.sebix.warsztat.model.Car;

import java.util.List;
@CrossOrigin
@RestController
public class CarController {

    @Autowired
    CarService carService;

    @GetMapping("/car/add/{name}/{model}/{marka}")
    public void addCar(@PathVariable("name") String name, @PathVariable("model") String model, @PathVariable("marka") String marka){
        carService.addCar(name, model, marka);
    }

    @GetMapping("/car/listall")
    public List<Car> getAllCars() {
        return carService.getAllCars();
    }

    @GetMapping("/car/remove/{nazwa}")
    public void remove(@PathVariable("nazwa") String name) {
        carService.removeCar(name);
    }

}
//good job
