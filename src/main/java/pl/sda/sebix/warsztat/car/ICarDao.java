package pl.sda.sebix.warsztat.car;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.sda.sebix.warsztat.model.Car;

@Repository
public interface ICarDao extends CrudRepository<Car, Long> {
}
