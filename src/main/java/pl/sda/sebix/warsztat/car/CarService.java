package pl.sda.sebix.warsztat.car;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import pl.sda.sebix.warsztat.model.Car;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class CarService {

    @Autowired
    SessionFactory sessionFactory;

    @Autowired
    ICarDao iCarDao;

    public void addCar(String name, String model, String marka) {
        Car car = new Car();
        car.setName(name);
        car.setMarka(marka);
        car.setModel(model);
        iCarDao.save(car);
    }

    public List<Car> getAllCars() {
        List<Car> list = new ArrayList<>();
        for (Car car : iCarDao.findAll()) {
            list.add(car);
        }
        return list;
    }

    @Transactional
    public void removeCar(String name) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("DELETE Car WHERE name = :name")
                .setParameter("name", name);
        query.executeUpdate();

    }


}
