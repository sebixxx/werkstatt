package pl.sda.sebix.warsztat.workshop;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import pl.sda.sebix.warsztat.model.Workshop;

import javax.transaction.Transactional;
import java.util.ArrayList;
@CrossOrigin
@RestController
public class WorkshopController {

    @Autowired
    IWorkshopDAO workshopDAO;



    @GetMapping("/workshop/list")
    @Transactional
    @ResponseBody
    public ResponseEntity<ArrayList<Workshop>> getAllWorkshops() {
        Iterable<Workshop> all = workshopDAO.findAll();
        ArrayList<Workshop> workshops = new ArrayList<>();
        for (Workshop w : all) {
            workshops.add(w);
        }
        return ResponseEntity.ok(workshops);
    }

    @PostMapping("/workshop/add")
    @Transactional
    @ResponseBody
    public ResponseEntity<Workshop> addWorkshop(@RequestBody Workshop workshop) {
        workshopDAO.save(workshop);
        return ResponseEntity.ok(workshop);
    }

    @GetMapping("/workshop/remove/{id:[0-9]+}")
    @Transactional
    @ResponseBody
    public ResponseEntity<Workshop> removeWorkshop(@PathVariable("id") String id, ModelMap model) {
        Workshop workshopToDelete = workshopDAO.findOne(Long.valueOf(id));
        workshopDAO.delete(Long.valueOf(id));
        return ResponseEntity.ok(workshopToDelete);
    }


}