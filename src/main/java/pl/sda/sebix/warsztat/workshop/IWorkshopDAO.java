package pl.sda.sebix.warsztat.workshop;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.sda.sebix.warsztat.model.Workshop;

@Repository
public interface IWorkshopDAO extends CrudRepository <Workshop, Long> {

}
